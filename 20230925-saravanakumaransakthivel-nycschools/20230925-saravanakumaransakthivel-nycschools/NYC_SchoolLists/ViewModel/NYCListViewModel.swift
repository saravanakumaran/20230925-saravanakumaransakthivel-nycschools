//
//  NYCListViewModel.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/25/23.
//

import Foundation

protocol NYCSchoolListDownloadable: AnyObject {
    func getListOfNYCSchoolList(for offSet: String) async
}

class NYCListViewModel {
    weak var nycListViewable: NYCSchoolListViewable?
    
    init(nycListViewable: NYCSchoolListViewable? = nil) {
        self.nycListViewable = nycListViewable
    }
    
}

extension NYCListViewModel: NYCSchoolListDownloadable {
    
    func getListOfNYCSchoolList(for offSet: String) async {
        let networkExecutor = NetworkExecutor()
        let result = await networkExecutor.getNYCSchoolList(offSet)
        switch result {
        case .success(let response):
            guard let nycListViewable = self.nycListViewable else {
                return
            }
            nycListViewable.reloadTableView(with: response)
        case .failure:
            break;
        }
    }
}
