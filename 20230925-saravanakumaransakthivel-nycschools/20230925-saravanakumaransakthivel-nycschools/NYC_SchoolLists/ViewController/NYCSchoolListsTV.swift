//
//  NYCSchoolListsTV.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/24/23.
//

import UIKit

protocol NYCSchoolListViewable : AnyObject {
    func fetchSchoolList() async
    func reloadTableView(with newData: [NYCSchoolModel])
    func loadMoreData()
}

class NYCSchoolListsTV: UITableViewController {
    
    var isLoading = false
    var dataSource = [NYCSchoolModel]()
    var viewModel: NYCListViewModel?
    
    override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        viewModel = NYCListViewModel()
        viewModel?.nycListViewable = self


        // Fetch the intial NYC school list and display in the tableview
        Task {
            await fetchSchoolList()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "NYC Schools"
    }
    
    internal func registerTableViewCell() {
        //Register Loading Cell
        let tableViewLoadingCellNib = UINib(nibName: "NYCLoadingCell", bundle: nil)
        self.tableView.register(tableViewLoadingCellNib, forCellReuseIdentifier: "loadingcellid")
        
        //Register school Cell
        let schoolCell = UINib(nibName: "NYCSchoolCell", bundle: nil)
        self.tableView.register(schoolCell, forCellReuseIdentifier: "schoolcellidentifier")
    }
}

extension NYCSchoolListsTV {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section == 0 else {
            return 1
        }
        return dataSource.count
    }
    
    override  func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "schoolcellidentifier", for: indexPath) as! NYCSchoolCell
            let schoolModel = self.dataSource[indexPath.row]
            let cellViewModel = NYCSchoolCellViewModel(nycSchoolModel: schoolModel)
            cell.accessoryType = .disclosureIndicator
            cell.selectionStyle = .none
            cell.configureView(cellViewModel)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "loadingcellid", for: indexPath) as! NYCLoadingCell
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.dataSource.count != 0 {
            let selectedSchoolModel = self.dataSource[indexPath.row]
            guard let satDetailVC = self.storyboard?.instantiateViewController(identifier: "satViewController") as? SATScoreDetailVC else {
                return
            }
            satDetailVC.schoolModel = selectedSchoolModel
            self.navigationController?.pushViewController(satDetailVC, animated: true)
        }
    }
    
    /*
        Implmeneted scrollview delegate to trigger load more data
     */
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading {
            loadMoreData()
        }
    }
    
    
}

extension NYCSchoolListsTV: NYCSchoolListViewable {
    
    /*  Below method helps to make service call through view model.
     */
    func fetchSchoolList() async {
        do {
            guard let viewModel = self.viewModel else {
                return
            }
            await viewModel.getListOfNYCSchoolList(for: String(dataSource.count+1))
        }
    }
    
    /*
        This method make sure new set of data is append with the existing data and reload table in main thread
     */
    func reloadTableView(with newData: [NYCSchoolModel]) {
        self.dataSource = self.dataSource + newData
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    /*
     This method helps to fetch next set of data when we do swipe up or when user taps on loading cell
     */
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            DispatchQueue.global().async {
                // Fake background loading task for 2 seconds
                sleep(2)
                // Download more data here
                Task {
                    await self.fetchSchoolList()
                }
                self.isLoading = false
            }
        }
    }
}
