//
//  SATScoreDetailModel.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/25/23.
//

import Foundation

/* This a model for the SAT Score, below are the data we get from the backend.
    - dbn
    - school_name - School's Name
    - num_of_sat_test_takers - overall students who took the test
    - sat_critical_reading_avg_score - Students avg SAT Critical reading scroe
    - sat_math_avg_score - Students avg on SAT math scroe
    - sat_writing_avg_score - Students avg on SAT writing scroe
    
    This Model conforms Codable protocol to decode JSON response to Model object.
 **/

struct SATScoreDetailModel: Codable {
    let dbn : String?
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
}
