//
//  SATScoreDetailVCViewController.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/25/23.
//

import UIKit

protocol SATScoreDetailViewable: AnyObject {
    func getSATScoreDetails() async
    func updateUIWithDetails() 
}

class SATScoreDetailVC: UIViewController {
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var noSATTestTakerLabel: UILabel!
    @IBOutlet weak var satCriticalAvgReading: UILabel!
    @IBOutlet weak var satAvgMathScore: UILabel!
    @IBOutlet weak var satAvgWritingScore: UILabel!
    @IBOutlet weak var stackView: UIStackView!

    var schoolModel: NYCSchoolModel?
    var satScoreViewModel: SATScoreDetailVCViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
        
        Task {
            await getSATScoreDetails()
        }
    }
    
    func setupUI() {
        NSLayoutConstraint.activate([
            schoolNameLabel.heightAnchor.constraint(equalTo: self.stackView.heightAnchor, multiplier: 0.1),
            noSATTestTakerLabel.heightAnchor.constraint(equalTo: self.stackView.heightAnchor, multiplier: 0.1),
            satCriticalAvgReading.heightAnchor.constraint(equalTo: self.stackView.heightAnchor, multiplier: 0.1),
            satAvgMathScore.heightAnchor.constraint(equalTo: self.stackView.heightAnchor, multiplier: 0.1),
            satAvgWritingScore.heightAnchor.constraint(equalTo: self.stackView.heightAnchor, multiplier: 0.1),
        ])
    }

}

extension SATScoreDetailVC: SATScoreDetailViewable {
    func getSATScoreDetails() async {
        satScoreViewModel = SATScoreDetailVCViewModel()
        satScoreViewModel?.satDetailViewable = self
        guard let schoolModel = schoolModel,
            let dbn = schoolModel.dbn else {
            return
        }
        await satScoreViewModel?.getSATScoreDetail(for: dbn)
    }
    
    func updateUIWithDetails() {
        guard let satScoreVM = self.satScoreViewModel else {
            return
        }
        DispatchQueue.main.async {
            self.schoolNameLabel?.text = satScoreVM.school_name
            self.descriptionLabel?.text = satScoreVM.overview_paragraph
            self.noSATTestTakerLabel?.text = satScoreVM.num_of_sat_test_takers
            self.satCriticalAvgReading?.text = satScoreVM.sat_critical_reading_avg_score
            self.satAvgMathScore?.text = satScoreVM.sat_math_avg_score
            self.satAvgWritingScore?.text = satScoreVM.sat_writing_avg_score
        }
    }
}
