//
//  SATScoreDetailVCViewModel.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/25/23.
//

import Foundation

protocol SATScoreDetailDownloadable: AnyObject {
    func getSATScoreDetail(for schoolDbn: String) async
    func updateViewModel(_ satScoreDetail: SATScoreDetailModel)
}


class SATScoreDetailVCViewModel {
    
    weak var satDetailViewable: SATScoreDetailViewable?
    
    var dbn : String?
    var school_name: String?
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
    var overview_paragraph: String?
    
    init() {
        
    }
}

extension SATScoreDetailVCViewModel: SATScoreDetailDownloadable {
    func getSATScoreDetail(for schoolDbn: String) async {
        let networkExecutor = NetworkExecutor()
        let result = await networkExecutor.getSATScore(schoolDbn)
        switch result {
        case .success(let response):
            guard !response.isEmpty else {
                updateUIWithError()
                return
            }
            updateViewModel(response[0])
        case .failure:
            updateUIWithError()
            break;
        }

    }
    
    func updateViewModel(_ satScoreDetail: SATScoreDetailModel) {
        
        guard let vc = satDetailViewable as? SATScoreDetailVC,
        let collegeModel = vc.schoolModel else {
            return
        }
        
        self.dbn = satScoreDetail.dbn
        self.school_name = satScoreDetail.school_name
        
        let totalSATTestTaker = satScoreDetail.num_of_sat_test_takers ?? "n/a"
        let criticalReadingAvgScore = satScoreDetail.sat_critical_reading_avg_score ?? "n/a"
        let mathAvgScore = satScoreDetail.sat_math_avg_score ?? "n/a"
        let writingAvgScore = satScoreDetail.sat_writing_avg_score ?? "n/a"
        
        
        self.num_of_sat_test_takers = "Total number of SAT Test Takers\n" + totalSATTestTaker
        self.sat_critical_reading_avg_score = "SAT Critical reading Avg. score\n" + criticalReadingAvgScore
        self.sat_math_avg_score = "SAT Math Avg. score\n" + mathAvgScore
        self.sat_writing_avg_score = "SAT Writing Avg.Score\n" + writingAvgScore
        self.overview_paragraph = collegeModel.overview_paragraph
        
        vc.updateUIWithDetails()

    }
    
    func updateUIWithError() {
        
        guard let vc = satDetailViewable as? SATScoreDetailVC else {
            return
        }
        
        self.num_of_sat_test_takers = "Something went wrong, Not able to get selected school SAT score details at this time please try again later"

        vc.updateUIWithDetails()
    }

}
