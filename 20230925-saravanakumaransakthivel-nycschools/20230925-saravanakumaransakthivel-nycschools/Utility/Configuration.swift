//
//  Configuration.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/24/23.
//

import Foundation

/*
    This is struct holds all constant variables.
 */

struct Configuration {
    // Http Schema
    static let httpSchema = "https"
    
    // Base URL
    static let baseUrl = "data.cityofnewyork.us"
    
    // Generated App token
    static let app_token = "vMDXTk4mgdbXMDHLHHHkPHMER"
    static let app_token_key = "X-App-Token"
    
    // NYC school list Path and Query
    static let path = "/api/id/s3k6-pzi2.json"
    static let selectQuery = "$select"
    static let selectQueryValue =  "`dbn`,`school_name`,`overview_paragraph`,`website`,`phone_number`,`fax_number`,`school_email`,`primary_address_line_1`,`city`,`zip`,`state_code`"
    static let limitQuery = "$limit"
    static let offsetQuer = "$offset"
    
    // SAT Score
    static let satScorePath = "/resource/f9bf-2cp4.json"
    static let satScoreQuery = "dbn"
    
}
