//
//  EndPointGenerator.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/24/23.
//

import Foundation

/*
    This protocol helps to construct final end point for both NYC school list and SAT score endpoint.
 */
protocol EndPointGenratorable {
    func getNYCSchoolEndPoint(offset:String) -> EndPoint
    func getSATScoreEndPoint(for schoolDbn: String) -> EndPoint
}

/*
    Endpoint structrue holds base path and its query parameter.
    Endpoint url is constructed from its base and query parameter.
 */
struct EndPoint {
    let path: String
    let queryItems: [URLQueryItem]
    
    // We still have to keep 'url' as an optional, since we're
    // dealing with dynamic components that could be invalid.
    var url: URL? {
        var components = URLComponents()
        components.scheme = Configuration.httpSchema
        components.host = Configuration.baseUrl
        components.path = path
        components.queryItems = queryItems
        return components.url
    }
}

struct EndPointGenerator : EndPointGenratorable {
    func getNYCSchoolEndPoint(offset: String) -> EndPoint {
        return EndPoint(path: Configuration.path,
                        queryItems: [URLQueryItem(name: Configuration.selectQuery,
                                                  value: Configuration.selectQueryValue),
                                     URLQueryItem(name: Configuration.limitQuery,
                                                  value: "10"),
                                     URLQueryItem(name: Configuration.offsetQuer,
                                                  value: offset)])
        
        
    }
    
    func getSATScoreEndPoint(for schoolDbn: String) -> EndPoint {
        return EndPoint(path: Configuration.satScorePath,
                        queryItems: [URLQueryItem(name: Configuration.satScoreQuery,
                                                  value: schoolDbn)])
    }
}
