//
//  NetworkExecutor.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/24/23.
//

import Foundation

/*
    This is an enum to group all network error and json parsing error.
    - invalidURL - indicates the constructed URL is not correct
    - serverError - This indicates the error return by server and it all also carries error paramter.
    - noData - Service call happened and for some reason if data is not present this type will be returned.
    - parsingError - If there is an error occured while parsing this will be returned
    
    Improvements : We can define other errors and seperate networks errors as different enum and parser errors as another enum group to give a clarity on what is realy failing.
 */

enum NetworkError: Error {
    case invalidURL
    case noData
    case parsingError
    case unknown
}

protocol NetworkExecutable {
    func getNYCSchoolList(_ offset: String) async -> Result<[NYCSchoolModel], NetworkError>
    func getSATScore(_ schoolDbn: String) async -> Result<[SATScoreDetailModel], NetworkError>
}


class NetworkExecutor : NetworkExecutable, HTTPClient {
    func getNYCSchoolList(_ offset: String) async -> Result<[NYCSchoolModel], NetworkError> {
        let endPointGenerator = EndPointGenerator()
        let endPoint = endPointGenerator.getNYCSchoolEndPoint(offset: offset)

        return await sendRequest(endpoint: endPoint,
                                 responseModel: [NYCSchoolModel].self)
    }
    
    func getSATScore(_ schoolDbn: String) async -> Result<[SATScoreDetailModel], NetworkError> {
        let endPointGenerator = EndPointGenerator()
        let satScoreEndPoint = endPointGenerator.getSATScoreEndPoint(for: schoolDbn)
        
        return await sendRequest(endpoint: satScoreEndPoint,
                                 responseModel:[SATScoreDetailModel].self)
    }
    
}
