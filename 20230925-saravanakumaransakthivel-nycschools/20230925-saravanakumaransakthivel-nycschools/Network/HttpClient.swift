//
//  HttpClient.swift
//  20230925-SaravanakumaranSakthivel-nycschools
//
//  Created by Saravanakumaran Sakthivel on 9/24/23.
//

import Foundation

protocol HTTPClient {
    func sendRequest<T: Decodable>(endpoint: EndPoint, responseModel: T.Type) async -> Result<T, NetworkError>
}

/**
    Basic generic implementation to load http calls and return response and network error.
 */

extension HTTPClient {
    func sendRequest<T: Decodable>(
        endpoint: EndPoint,
        responseModel: T.Type
    ) async -> Result<T, NetworkError> {
        
        guard let url = endpoint.url else {
            return .failure(.invalidURL)
        }
        
        var request = URLRequest(url: url)
        request.setValue(Configuration.app_token, forHTTPHeaderField: Configuration.app_token_key)
        
        do {
            let (data, response) = try await URLSession.shared.data(for: request, delegate: nil)
            guard let response = response as? HTTPURLResponse else {
                return .failure(.noData)
            }
            switch response.statusCode {
            case 200...299:
                guard let decodedResponse = try? JSONDecoder().decode(responseModel, from: data) else {
                    return .failure(.parsingError)
                }
                return .success(decodedResponse)
            case 401:
                return .failure(.unknown)
            default:
                return .failure(.unknown)
            }
        } catch {
            return .failure(.unknown)
        }
    }
}
