//
//  ConfigurationTests.swift
//  20230925-SaravanakumaranSakthivel-nycschoolsTests
//
//  Created by Saravanakumaran Sakthivel on 9/26/23.
//

import XCTest

final class ConfigurationTests: XCTestCase {
    
    func testCheckHttpSchema () {
        XCTAssertEqual(Configuration.httpSchema, "https", "Http Schema should be https.")
    }
    
    func testCheckBaseURL () {
        XCTAssertEqual(Configuration.baseUrl, "data.cityofnewyork.us", "Base URL for the API calls are not mathing.")
    }
    
    func testCheckAppToken() {
        XCTAssertEqual(Configuration.app_token, "vMDXTk4mgdbXMDHLHHHkPHMER" , "Api token is not matching.")
    }
    
    func testCheckSchoolAPIPath() {
        XCTAssertEqual(Configuration.path, "/api/id/s3k6-pzi2.json" , "NYC Schools api path is not correct.")
    }
    
    func testCheckSATScorePath() {
        XCTAssertEqual(Configuration.satScorePath, "/resource/f9bf-2cp4.json" , "SAT Score paths is not correct.")
    }
    
}
