# 20230925-SaravanakumaranSakthivel-NYCSchools

## Project Name : 20230925-SaravanakumaranSakthivel-NYCSchools

## About the project :
    Native iOS app to provide information on NYC High school.

## Design Pattern Used:
    - MVVM 
    - Protocol oriented approach. 
    - async/ await for asynchronous programming.

## Feature 1: 

## List all NYC Schools :
        - This feature helps to list all NYC schools in the tableview. We are connecting to below web service to download list of schools. Implemented pagination to load the list of schools. I set pagination offset value as 10, One service call will give us 10 records at a time. 

        - Implemented infite scrolling tableview to load the NYC school list.

        - While scrolling down if we reached end of the screen, am checking the scroll positoin and offset y position of scrollview and calling service to fetch next set of records. Pulling 10 records at a time and concatenate with the existing records and refreshing the data.

## Concurrency Technique used: 
        - Used latest async/await for the asynchronous API call to download the data.

## API Details: 

## Base URL : 
        https://data.cityofnewyork.us/api/id/s3k6-pzi2.json?

## Querey Parameters: 

        dbn, school_name, overview_paragraph, website, phone_number, fax_number, school_email, primary_address_line_1, city, zip, state_code, offset and limit

## Sample Response: 

        [{"dbn":"02M139","school_name":"Stephen T. Mather Building Arts & Craftsmanship High School","boro":"M","overview_paragraph":"Are you hoping four years of high school will lead you to college, a challenging career, or both? Do you like projects, working with your hands, and learning by doing? How about field trips and exploring outdoors all year? Are you creative, inquisitive, action-oriented, responsible, a positive risk-taker, collaborative, and conscientious? At our school you will prepare for college and beyond through hands-on/minds-on learning and skills training. Partnered with the National Park Service (NPS), our school offers CTE pathways in the specialized building arts or landscape stewardship, all through the lens of historic preservation. You will build a strong academic foundation and learn solid trade skills, opening doors to a successful future.","website":"www.MatherHSNYC.org","phone_number":"212-399-3520","fax_number":"212-245-4669","school_email_1":"school_email","primary_address_line_1_1":"primary_address_line_1","city_1":"city","zip_1":"zip","state_code_1":"state_code"}]
 
## Feature 2: 

## Display SAT Score Details: 

        - Created a View controller to display selected NYC school SAT score information. 
        - Displayed be below information in the screen.
                - School Name
                - Number os SAT test takers 
                - SAT Critical Reading Avg Score.
                - SAT Math Avg SCroe.
                - SAT Writing Avg Score.
                - College Description. 
## Base URL : 
        
        https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=08X282

## Querey Parameters: 

        dbn, school_name, num_of_sat_test_takers, sat_critical_reading_avg_score, sat_math_avg_score, sat_writing_avg_score.

## Sample Response :
        [{"dbn":"08X282","school_name":"WOMEN'S ACADEMY OF EXCELLENCE","num_of_sat_test_takers":"44","sat_critical_reading_avg_score":"407","sat_math_avg_score":"386","sat_writing_avg_score":"378"}]


## Test cases :
        Added a test cases for below files.
        - ConfigurationTests
        - NYCSchoolCellViewModelTests


## Planned Improvements and Enhancements: 
        - Write unit test case for all the classes.
        - Adding feature to view recently viewed School SAT score details.
        - UI improvements, Displaying error message if there is no data return from backend.
